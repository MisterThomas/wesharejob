import React, {Component} from "react";
import {Navbar, Nav, Image} from 'react-bootstrap';
//import { Link } from 'react-router-dom';
// get our fontawesome imports
import { faHome } from "@fortawesome/free-solid-svg-icons";
import { faLaptopCode} from "@fortawesome/free-solid-svg-icons";
import { faCode} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import './HeaderNavbar.css'
import ListOfferFavorite from "./Offer/ListOfferFavorite";
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import UserService from "../services/UserService";



 class HeaderNavbar extends Component {


     constructor(props) {
         super(props)
         this.state = {
             username: UserService.getLoggedInUserName().username,
         };
     }

     hide (elements) {
         elements = elements.length ? elements : [elements];
         for (var index = 0; index < elements.length; index++) {
             elements[index].style.display = 'none';
         }
     }


    render() {
        const isUserLoggedIn = UserService.getLoggedInUserName().username;
        console.log(isUserLoggedIn)
        return (
         <Navbar default collapseOnSelect expand="lg" bg="primary" variant="light">
            <Navbar.Brand  className="Navbar" variant="warning" href="/">
               {/* <b className="we">W</b><b className="share">S</b><b className="job">J</b></Navbar.Brand>*/}
             <div>
                 <Image className="picture" src="https://res.cloudinary.com/ddumauwoh/image/upload/v1632289863/WeShareJob_logo_1_peqlmw.png"></Image>
          {/*       <FontAwesomeIcon icon={faLaptopCode} />
                 <FontAwesomeIcon icon={faCode} />*/}
             </div>
            </Navbar.Brand>
         {/*    <img  className="picture"  src={ require('./public/WSJ.png') } />*/}
             <Navbar.Toggle />
                <Navbar.Collapse>
               <Nav className="mr-auto">
                <Nav.Link className="navlink"  id="black"  href="/">Home</Nav.Link>

                   {/*{!isUserLoggedIn && <Nav.Link className="navlink" id="black" href="/signup">SignUp</Nav.Link>}*/}


               {/*   {isUserLoggedIn && <Nav.Link className="navlink" id="black" href="/account">Account</Nav.Link>}*/}

                    <Nav.Link className="navlink" id="black"  href="/listoffer">List Offers</Nav.Link>
{/*                   <Switch>
                   {isUserLoggedIn && <Nav.Link className="navlink" id="black"   href={"/listOfferFavorite/" + this.state.username}>List Offers Favorite</Nav.Link> }
                   </Switch>
                   {!isUserLoggedIn && <Nav.Link className="navlink" id="black" href="/login">Login</Nav.Link>}

                   {isUserLoggedIn && <Nav.Link className="navlink" id="red" onClick={UserService.logOut}>Logout</Nav.Link>}*/}
            </Nav>
                </Navbar.Collapse>
        </Navbar>
        )
    }
}
export default HeaderNavbar;