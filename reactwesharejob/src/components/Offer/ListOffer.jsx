import React, { Component } from 'react'
import OfferService from "../../services/OfferService";
import "./css/ListOffer.css"
//import Offers from "./Offers";
import axios from 'axios';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
//import {faList, faEdit, faTrash, faStepBackward, faFastBackward, faStepForward, faFastForward, faSearch, faTimes} from '@fortawesome/free-solid-svg-icons';
import {faList, faEdit, faTrash, faSearch, faTimes, faSave, faEnvelopeOpenText, faLink} from '@fortawesome/free-solid-svg-icons';
import {Card, Button, InputGroup, FormControl} from 'react-bootstrap';
//import OfferFavoriteService from "../../services/OfferFavoriteService";
import MyToast from "./MyToast";
import OfferFavoriteService from "../../services/OfferFavoriteService";
import UserService from "../../services/UserService";

class ListOffer extends Component {


    constructor(props) {



        var today = new Date();
        var dd = today.getDate();

        var mm = today.getMonth() + 1;
        var yyyy = today.getFullYear();
        if (dd < 10) {
            dd = '0' + dd;
        }

        if (mm < 10) {
            mm = '0' + mm;
        }
        var dd = today.getDate();

        var mm = today.getMonth() + 1;
        var yyyy = today.getFullYear();


        if (dd < 10) {
            dd = '0' + dd;
        }

        if (mm < 10) {
            mm = '0' + mm;
        }

        today = dd + '/' + mm + '/' + yyyy;

        super(props)

        this.state = {
            id: this.props.match.params.id,
            listOffers: [],
            search: '',
            username: UserService.getLoggedInUserName().username,
            date_scrap_offer: today,
            is_active: false,
            showHideDemo1: false,

        }


        this.addOffer = this.addOffer.bind(this);
        this.saveOfferFavorite = this.saveOfferFavorite.bind(this);
        this.updateOffer = this.updateOffer.bind(this);
        this.deleteOffer = this.deleteOffer.bind(this);
        this.search = this.search.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }





    /*
        sortData = () => {
            setTimeout(() => {
                this.state.sortDir === "asc" ? this.setState({sortDir: "desc"}) : this.setState({sortDir: "asc"});
                this.props.listOffers(this.state.currentPage);
            }, 500);
        };
    */

    componentDidMount() {

        OfferService.getAllOffers().then((res) => {
            this.setState({listOffers: res.data});

        });
    }

    handleChange(event) {
        this.setState({
                [event.target.name]: event.target.value
            }
        )
    }

    /*    addOfferFavorite(){
            this.props.history.push('/add-offerFavorite/_add');
        }*/

    addOffer() {
        if (this.state.username === "admin") {
            this.props.history.push('/add-offer/_add');
        }
    }

    offerId(id) {
        this.props.history.push(`/view-offer/${id}`);
    }

    updateOffer(id) {
        if (this.state.username === "admin") {
        this.props.history.push(`/add-offer/${id}`);
    }
    }

    deleteOffer(id) {
        if (this.state.username === "admin") {
            OfferService.deleteOffer(id).then(res => {
                this.setState({listOffers: this.state.listOffers.filter(offer => offer.id !== id)});
            });
        }
    }


    saveOfferFavorite = (event, idoffer) => {
  // saveOfferFavorite(id) {

        console.log(idoffer)
         let  offerFavorite = {
             date_scrap_offer:  this.state.date_scrap_offer,
             username: this.state.username,
             is_active: this.state.is_active,
             id: this.props.id,
         }
            return OfferFavoriteService.offerFavoritePost(idoffer, offerFavorite).then(res => {
                this.props.history.push('/listoffer');
            });
    }


/*    saveOfferFavorite = (e) => {
        e.preventDefault();

        console.log('offer => ' + JSON.stringify(offerFavorite));

        if(this.state.listOffers.offer.id === this.state.listOffers.offer.find(this.state.listOffers.offer.id)) {
            OfferFavoriteService.offerFavoritePost(this.state.listOffers[this.state.id].offer.id, offerFavorite).then(res =>{
                this.props.history.push('/listoffer');
            });
        } else {
            this.props.history.push('/listoffer');
        }
    }*/

    search(){
        axios.get(`http://localhost:8085/api/offers/listOffers/search`, {
            params: {
                city: this.state.search,
                company: this.state.search,
                post: this.state.search
            }
        })
            .then(response => response.data)
            .then((data) => {
                this.setState({listOffers: data});
            });
    }

    cancelSearch = () => {
        this.setState({"search" : ''});
         OfferService.getAllOffers().then((res) => {
            this.setState({ listOffers: res.data});

        });
    };



        render() {
            const {offers, currentPage, totalPages, search} = this.state;
            return (
                <div>
                    <h2 className="text-center"> <FontAwesomeIcon icon={faList} />List Offer</h2>
                    <div style={{"display":this.state.show ? "block" : "none"}}>
                        <MyToast show = {this.state.show} message = {"Offer Deleted Successfully."} type = {"danger"}/>
                    </div>
                    <Card>
                        <Card.Header>
                    <div style={{"float":"right"}}>
                        <InputGroup size="sm">
                            <FormControl placeholder="Search" name="search"
                                         className={"info-border bg-dark text-white"} value={this.state.search}  onChange={this.handleChange}/>
                            <InputGroup.Append>
                                <Button id="addButton" size="sm" variant="outline-info" type="button"  onClick={this.search}>
                                    <FontAwesomeIcon icon={faSearch}/>
                                </Button>
                                <Button size="sm" variant="outline-danger" type="button" onClick={this.cancelSearch}>
                                    <FontAwesomeIcon icon={faTimes} />
                                </Button>
                            </InputGroup.Append>
                        </InputGroup>
                    </div>
                        </Card.Header>
                        {this.state.username === "admin" ? (
                    <div  className="row1">
                        <button  size="sm" className="btn btn-primary" onClick={this.addOffer}><FontAwesomeIcon icon={faSave}/> Add Offer</button>
                    </div>
                        ):null}
                    <br></br>
                    <div className="row1">
                        <table className="table table-striped table-bordered">
                            <thead>
                            <tr>
                                <th> Company</th>
                                <th> City</th>
                                <th> post</th>
                                <th> url website</th>
                            </tr>
                            </thead>
                            <section className="jobs" id="category-2">
                                <article>
                                    <h2></h2>
                            <tbody>
                            {
                                this.state.listOffers.map(
                                    offer =>
                                        <ul key={offer.id}>
                                            <li> {offer.company}</li>
                                            <li> {offer.city}</li>
                                            <li> {offer.post}</li>
                                            <li><button  className="btn btn-info"><FontAwesomeIcon icon={faLink} /><a href={offer.url}>url</a></button></li>
                                            <li>
                                                {this.state.username === "admin" ? (
                                                <button onClick={() => this.updateOffer(offer.id)}
                                                        className="btn btn-info"><FontAwesomeIcon icon={faEdit} />Update Offer
                                                </button>
                                                ) :null }
                                                {this.state.username === "admin" ? (
                                                <button style={{marginLeft: "10px"}}
                                                        onClick={() => this.deleteOffer(offer.id)}
                                                        className="btn btn-danger"><FontAwesomeIcon icon={faTrash} />Delete
                                                </button>
                                                ):null}
                                                <button style={{marginLeft: "10px"}}
                                                        onClick={() => this.offerId(offer.id)}
                                                        className="btn btn-info"><FontAwesomeIcon icon={faEnvelopeOpenText} />View
                                                </button>

                                                {this.state.username === "admin" ? (
                                               <button style={{marginLeft: "10px"}} style={{marginLeft: "10px", display: this.state.username === "admin" ? "block" : "none" }}
                                                       onClick={(event) => this.saveOfferFavorite(event, offer.id)}
                                                   //      onClick={() => this.saveOfferFavorite(offer.id)}
                                                        className="btn btn-info"><FontAwesomeIcon icon={faSave} /> add OfferFavorite
                                                </button>
                                                ):null}
                                                {/*<button style={{marginLeft: "10px"}} onClick={ () => this.addOfferFavorite(offer.id)} className="btn btn-info">Add OfferFavorite</button>*/}
                                            </li>
                                        </ul>
                                )
                            }
                            </tbody>
                                </article>
                            </section>
                        </table>
                    </div>
                    </Card>
                </div>
            )
        }
}
export default ListOffer;