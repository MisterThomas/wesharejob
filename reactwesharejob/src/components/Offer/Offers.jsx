const Offers = ({stats}) => {
    return(
        <div className='offers'>
            <h2>{stats.Offers}</h2>
            <div className='describe'>
                <p>{`company : ${stats.company}`}</p>
                <p>{`city : ${stats.city}`}</p>
                <p>{`post : ${stats.post}`}</p>
                <p>{`url : ${stats.url}`}</p>
            </div>
        </div>
    )
}

export default Offers;