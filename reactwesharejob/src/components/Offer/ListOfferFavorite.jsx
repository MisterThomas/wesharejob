import React, {Component} from "react";
import OfferFavoriteService from "../../services/OfferFavoriteService";
import data from "bootstrap/js/src/dom/data";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faLink} from "@fortawesome/free-solid-svg-icons";
import UserService from "../../services/UserService";

class ListOfferFavorite extends Component {




    constructor(props) {



        super(props)
        this.state = {
            listOffersFavorite: [],
            username: props.match.params.name,
            id: this.props.match.params.id,
            };


        if (UserService.getLoggedInUserName().username != this.state.username) {
            this.props.history.push('/listoffer');
        }

        this.deleteOfferFavorite = this.deleteOfferFavorite.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }


    componentDidMount() {
        OfferFavoriteService.getOfferFavoriteUsername(this.state.username).then((res) => {
            console.log(res.data)
                this.setState({listOffersFavorite: res.data});
            });

        const rememberMe = localStorage.getItem('rememberMe') === 'true';
        const user = rememberMe ? localStorage.getItem('user') : '';
        this.setState({ user, rememberMe });
    }



    handleChange(event) {
        this.setState({
                [event.target.name]
                    :event.target.value
            }
        )
    }


    deleteOfferFavorite(id) {
        OfferFavoriteService.deleteOfferFavorite(id).then(res => {
            this.setState({listOffersFavorite: this.state.listOffersFavorite.filter(offerFavorite => offerFavorite.id !== id)});
        });
    }

    offerFavoritesId(id) {
        this.props.history.push(`/view-offer/${id}`);
    }

    render() {
       console.log(this.state.listOffersFavorite)

                return (
                    <div>
                        <h2 className="text-center">Offer ListFavorites by {this.props.match.params.name}</h2>
                        <br></br>
                        <div className="row">
                            <table className="table table-striped table-bordered">

                                <thead>
                                <tr>
                                    <th> Company</th>
                                    <th> City</th>
                                    <th> post</th>
                                    <th> url website</th>
                                </tr>
                                </thead>
                                <tbody>
                                    {/*    {


                                    <tr>
                                        {this.state.listOffersFavorites.map((item, index) => (
                                            <div key={index}>
                                                <td>{item.company}</td>
                                                <td>{item.city}</td>
                                                <td>{item.post}</td>
                                                <td>{item.url}</td>
                                            </div>
                                        ))}
                                    </tr>
                                }*/}
                                    {
        this.state.listOffersFavorite.map(
            item =>
                <tr key={item.id}>
                    <td> {item.offer.company}</td>
                    <td> {item.offer.city}</td>
                    <td> {item.offer.post}</td>
                    <td><button  className="btn btn-info"><FontAwesomeIcon icon={faLink} /><a href={item.offer.url}>url</a></button></td>
                    <td>
                        <button style={{marginLeft: "10px"}}
                                onClick={() => this.deleteOfferFavorite(item.id)}
                                className="btn btn-danger">Delete
                        </button>
                        <button style={{marginLeft: "10px"}}
                                onClick={() => this.offerFavoritesId(item.offer.id)}
                                className="btn btn-info">View
                        </button>
                    </td>
                </tr>
        )
    }
                        </tbody>
                    </table>

                </div>

            </div>
        )
    }
}
export default ListOfferFavorite;

