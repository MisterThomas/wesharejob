import React, { Component } from 'react'
import OfferService from "../../services/OfferService";
import "./css/AddOffer.css"
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faEdit, faSave , faUndo} from '@fortawesome/free-solid-svg-icons';
import UserService from "../../services/UserService";


class AddOffer extends Component {





    constructor(props) {
        super(props)


        //const today = new Date(),

        var today = new Date();
        var dd = today.getDate();

        var mm = today.getMonth()+1;
        var yyyy = today.getFullYear();
        if(dd<10)
        {
            dd='0'+dd;
        }

        if(mm<10)
        {
            mm='0'+mm;
        }
        var dd = today.getDate();

        var mm = today.getMonth()+1;
        var yyyy = today.getFullYear();


        if(dd<10)
        {
            dd='0'+dd;
        }

        if(mm<10)
        {
            mm='0'+mm;
        }

        today = dd+'/'+mm+'/'+yyyy;


        this.state = {

            // step 2
            id: this.props.match.params.id,
            company: '',
            city: '',
            post: '',
            url: '',
            username: UserService.getLoggedInUserName().username,
            is_active: false,
            date_scrap_offer: today,


        }

        console.log(this.state.id)
        this.changeCompanyHandler = this.changeCompanyHandler.bind(this);
        this.changeCityHandler = this.changeCityHandler.bind(this);
        this.changePostHandler = this.changePostHandler.bind(this);
        this.changeUrlHandler = this.changeUrlHandler.bind(this);
        this.saveOrUpdateOffer = this.saveOrUpdateOffer.bind(this);
    }

    // step 3
    UpdateOffer;
    componentDidMount(){

        // step 4
        if(this.state.id === '_add'){
            return
        }else{
            OfferService.getOffer(this.state.id).then( (res) =>{
                let offer = res.data;
                this.setState({
                    id : offer.id,
                    company: offer.company,
                    city: offer.city,
                    post: offer.post,
                    url : offer.url,


                });


                const rememberMe = localStorage.getItem('rememberMe') === 'true';
                const user = rememberMe ? localStorage.getItem('user') : '';
                this.setState({ user, rememberMe });

            });
        }
    }


    saveOrUpdateOffer = (e) => {
        e.preventDefault();
        let offer = {
            company: this.state.company,
            post: this.state.post,
            city: this.state.city,
            url: this.state.url,
            is_active: this.state.is_active,
            date_scrap_offer:  this.state.date_scrap_offer,
            username: this.state.username
        };
        console.log('offer => ' + JSON.stringify(offer));

        // step 5
        if(this.state.id === '_add'){
            OfferService.saveOffer(offer).then(res =>{
                this.props.history.push('/listoffer');
            });
        }else{
            OfferService.updateOffer(offer, this.state.id).then( res => {
                this.props.history.push('/listoffer');
            });
        }

    }

    changeCompanyHandler = (event) => {
        this.setState({company: event.target.value});
    }

    changeCityHandler = (event) => {
        this.setState({city: event.target.value});
    }

    changePostHandler = (event) => {
        this.setState({post: event.target.value});
    }

    changeUrlHandler = (event) => {
        this.setState({url: event.target.value});
    }

    cancel(){
        this.props.history.push('/listOffer');
    }

    getTitle(){
        if(this.state.id === '_add'){
            return <h1 className="text-center"><FontAwesomeIcon icon={faSave} /> Add offer</h1>
        }else{
            return <h1 className="text-center"><FontAwesomeIcon icon={faEdit} /> Update offer</h1>
        }
    }
    render() {
        console.log(this.state)

        console.log(this.state.username)

        return (
            <div>
                {
                    this.getTitle()
                }
                <br></br>
                <div className = "container">
                    <div className = "row">
                        <div className = "card col-md-6 offset-md-3 offset-md-3">
                            <div className = "card-body">
                                <form>
                                    <div className = "form-group">
                                        <label> First Name: </label>
                                        <input placeholder="Company" name="company" className="form-control"
                                               value={this.state.company} onChange={this.changeCompanyHandler}/>
                                    </div>
                                    <div className = "form-group">
                                        <label> Last Name: </label>
                                        <input placeholder="City" name="city" className="form-control"
                                               value={this.state.city} onChange={this.changeCityHandler}/>
                                    </div>
                                    <div className = "form-group">
                                        <label> Post: </label>
                                        <input placeholder="Post" name="post" className="form-control"
                                               value={this.state.post} onChange={this.changePostHandler}/>
                                    </div>
                                    <div className = "form-group">
                                        <label> Url: </label>
                                        <input placeholder="url" name="url" className="form-control"
                                               value={this.state.url} onChange={this.changeUrlHandler}/>
                                    </div>
                                    <button className="btn btn-success" onClick={this.saveOrUpdateOffer}><FontAwesomeIcon icon={faSave} /> Save</button>
                                    <button className="btn btn-danger" onClick={this.cancel.bind(this)} style={{marginLeft: "10px"}}><FontAwesomeIcon icon={faUndo} /> Reset</button>
                                </form>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        )
    }
}
export default AddOffer;