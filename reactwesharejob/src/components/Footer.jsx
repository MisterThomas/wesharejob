import React, {Component} from "react";
import {Button, Col, Container, Jumbotron} from "react-bootstrap";
import {Link} from "react-router-dom";
import './Footer.css'


export class Footer extends Component {
    constructor(props) {
        super(props)

        this.state = {

        }
    }

    render() {
        return (
            <div>
                <footer>
                    <div id="footer-left">
                                <img id="footer-logo" src=""/>
                        <h1>WeShareJob</h1>
                        <p id="footer-network"> We help employers to find The best Developers, share, and
                            knowledge as fundamental values when hiring through We Share Job.
                        </p>

                        <div id="mobile-footer">
                          {/*      <b id="total">21,704</b>*/}
                              {/*  <b id="jobs-posted"> Remote jobs posted</b>*/}
                                <li id="copyright"> Copyright © 2021 wesharejob
                                    <span id="mobile-break"></span>
                                </li>
                        </div>
                    </div>
                    </footer>
                </div>

    )}}
export default Footer