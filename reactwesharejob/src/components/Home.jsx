import React, {Component} from 'react'
import { Link } from 'react-router-dom';
import {Jumbotron, Row, Col, Image, Button, Container, Card, InputGroup, FormControl} from 'react-bootstrap';
import  './Home.css'
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
//import { ReactSearchAutocomplete } from 'react-search-autocomplete'

import {
    faEdit,
    faEnvelopeOpenText,
    faLink,
    faList,
    faSave,
    faSearch,
    faTimes,
    faTrash
} from "@fortawesome/free-solid-svg-icons";
import MyToast from "./Offer/MyToast";
import UserService from "../services/UserService";
import OfferFavoriteService from "../services/OfferFavoriteService";
import OfferService from "../services/OfferService";
import axios from "axios";

 const dictionary = {
    words: ['Paris','Marseille','Lille','Lyon','Bordeaux','Rouen', 'Strasbourg','Caen','Brest','lorient','paris','marseille','lille','lyon','bordeaux','rouen','strasbourg','caen','brest','lorient']
}


export  class Home extends Component {



    constructor(props) {

        var today = new Date();
        var dd = today.getDate();

        var mm = today.getMonth() + 1;
        var yyyy = today.getFullYear();
        if (dd < 10) {
            dd = '0' + dd;
        }

        if (mm < 10) {
            mm = '0' + mm;
        }
        var dd = today.getDate();

        var mm = today.getMonth() + 1;
        var yyyy = today.getFullYear();


        if (dd < 10) {
            dd = '0' + dd;
        }

        if (mm < 10) {
            mm = '0' + mm;
        }

        today = dd + '/' + mm + '/' + yyyy;

        super(props)

        this.state = {
            id: this.props.match.params.id,
            listOffers: [],
            search: '',
            username: UserService.getLoggedInUserName().username,
            date_scrap_offer: today,
            is_active: false,

        }
      //  const [prefix, setPrefix] = useState("");
     //   const [suggestion, setSuggestion] = useState("");


        this.saveOfferFavorite = this.saveOfferFavorite.bind(this);
        this.search = this.search.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    search(){
        axios.get(`http://localhost:8085/api/offers/listOffers/search`, {
            params: {
                city: this.state.search,
                company: this.state.search,
                post: this.state.search
            }
        })
            .then(response => response.data)
            .then((data) => {
                this.setState({listOffers: data});
            });
    }



    saveOfferFavorite = (event, idoffer) => {
        // saveOfferFavorite(id) {

        console.log(idoffer)
        let  offerFavorite = {
            date_scrap_offer:  this.state.date_scrap_offer,
            username: this.state.username,
            is_active: this.state.is_active,
            id: this.props.id,
        }
        return OfferFavoriteService.offerFavoritePost(idoffer, offerFavorite).then(res => {
            this.props.history.push('/listoffer');
        });
    }

    componentDidMount() {

        OfferService.getAllOffers().then((res) => {
            this.setState({listOffers: res.data});

        });
    }

/*
     onChange = (event) => {
        var value =  event.target.value;
        setPrefix(value);
        var words = value.split(" ");
        var trie_prefix = words[words.length - 1].toLowerCase();
        var found_words = myTrie.find(trie_prefix).sort((a, b) => {
            return a.length - b.length;
        });
        var first_word = found_words[0];
        if (
            found_words.length !== 0 &&
            value !== "" &&
            value[value.length - 1] !== " "
        ) {
            if (first_word != null) {
                var remainder = first_word.slice(trie_prefix.length);
                setSuggestion(value + remainder);
            }
        } else {
            setSuggestion(value);
        }
    };

     handleKeyDown = (e) => {
        if (e.keyCode === 39) {
            setPrefix(suggestion);
        }
    };
*/


    handleChange = (event) => {
        this.setState({
               [event.target.name]: event.target.value
               //  setPrefix(event.target.name);
            }
        )
    }


    offerId(id) {
        this.props.history.push(`/view-offer/${id}`);
    }

    render() {
        const {offers, currentPage, totalPages, search} = this.state;
        return (
            <div>
                <div style={{"display":this.state.show ? "block" : "none"}}>
                </div>
                <Container fluid>
                    <Jumbotron className="containerwescrapjob">
                        <h1 className="titre">WeShareJob</h1>
                        <p className="description">
                            This is marketplace we scrap job tech offer. We scrap and share 2000 new offer by day. You can find
                            offer of pole emploi, welcome to jungle, indeed,Linkedin.
                        </p>
                        <Link to="/listoffer">
                            <Button className="btn btn-primary" id="listoffer">Find Offer</Button>
                        </Link>
                    </Jumbotron>
                </Container>
                <Card>
                    <Card.Header>
                        <div style={{"float":"center"}}>
                            <div id="customBar">
                            <InputGroup size="sm">
                                <FormControl placeholder="Search" name="search"
                                             className={"info-border bg-white text-bue"} value={this.state.search} id="searchInput"  onChange={this.handleChange}/>
                                <InputGroup.Append>
                                    <Button id="addButton" size="sm" variant="outline-info" type="button"  onClick={this.search}>
                                        <FontAwesomeIcon icon={faSearch}/>
                                    </Button>
                                    <Button size="sm" variant="outline-danger" type="button" onClick={this.cancelSearch}>
                                        <FontAwesomeIcon icon={faTimes} />
                                    </Button>
                                </InputGroup.Append>
                            </InputGroup>
                            </div>
                        </div>
                    </Card.Header>
                    <br></br>
                    <div className="row">
                        <table className="table table-striped table-bordered">
                            <section id="category-17" className="jobs">
                                <article>
                                    <h2></h2>
                            <tbody className="dimmensionoffer">
                            {
                                this.state.listOffers.map(
                                    offer =>
                                        <ul  id="designofferId"className="designoffer" key={offer.id}>
                                            <li className="offercompanycss"> {offer.company} / {offer.city}</li>
                                            <li className="offerpost">Company : {offer.post}</li>
                                            <li className="offerdate">
                                                {new Intl.DateTimeFormat("en-GB", {
                                                    year: "numeric",
                                                    month: "long",
                                                    day: "2-digit"
                                                }).format(offer.date_scrap_offer)}
                                            </li>
                                            <li><button id="btnId" className="btn btn-info"><FontAwesomeIcon icon={faLink} /><a  id="linkUrl"href={offer.url}>url</a></button></li>
                                            <li>
                                                <button id="btnView" style={{marginLeft: "10px"}}
                                                        onClick={() => this.offerId(offer.id)}
                                                        className="btn btn-info"><FontAwesomeIcon icon={faEnvelopeOpenText} />View
                                                </button>
                                                {this.state.username === "admin" ? (
                                                <button id="btnOfferFavorite" style={{marginLeft: "10px"}}
                                                        onClick={(event) => this.saveOfferFavorite(event, offer.id)}
                                                        className="btn btn-info"><FontAwesomeIcon icon={faSave} /> add OfferFavorite
                                                </button>
                                                    ):null}
                                            </li>
                                        </ul>
                                )
                            }
                            </tbody>
                                </article>
                            </section>
                        </table>
                    </div>
                </Card>
            </div>
        )
    }
}






