import axios from "axios";


const  urlRegister = "http://localhost:8084/api/";
const  urlAuth = "http://localhost:8084";



export const USER_NAME_SESSION_ATTRIBUTE_NAME = 'authenticatedUser';
class Authentication  {



    login(username, password) {
        return axios
            .post(urlRegister + "signin", {
                username,
                password
            })
            .then(response => {
                if (response.data.accessToken) {
                    localStorage.setItem("user", JSON.stringify(response.data));
                }

                return response.data;
            });
    }


    register(username, password) {
        return axios
            .post(urlRegister  + "register", {
                username,
                password
            })
            .then(response => {
                if (response.data.accessToken) {
                    localStorage.setItem("AuthenticatedUser", JSON.stringify(response.data));
                }

                return response.data;
            });
    }


    signup(username, password) {
        return axios
            .post(urlRegister  + "signup", {
                username,
                password
            })
            .then(response => {
                if (response.data.accessToken) {
                    localStorage.setItem("user", JSON.stringify(response.data));
                }

                return response.data;
            });
    }

    registerSuccessfulLogin(username, password){
        console.log('register success')
        sessionStorage.setItem('authenticatedUser', username);
    }


    registerSuccessfulLoginForJwt(username, token) {
        sessionStorage.setItem(USER_NAME_SESSION_ATTRIBUTE_NAME, username)
        this.setupAxiosInterceptors(this.createJWTToken(token))
    }


    setupAxiosInterceptors(token) {

        axios.interceptors.request.use(
            (config) => {
                if (this.isUserLoggedIn()) {
                    config.headers.authorization = token
                }
                return config
            }
        )
    }


    createJWTToken(token) {
        return 'Bearer ' + token
    }

    logout(){
        sessionStorage.removeItem(USER_NAME_SESSION_ATTRIBUTE_NAME);
    }

    isUserLoggedIn() {
        let user = sessionStorage.getItem(USER_NAME_SESSION_ATTRIBUTE_NAME)
        if (user === null) { return null }
            return true
    }


    getLoggedInUserName() {
        let user = sessionStorage.getItem(USER_NAME_SESSION_ATTRIBUTE_NAME)
        if (user === null) return ''
        return user
    }


    executeBasicAuthenticationService(username, password) {
        return axios.get(`${urlAuth}/api/users`,
            {headers: {authorization: this.createBasicAuthToken(username, password)}})
    }

    executeJwtAuthenticationService(username, password) {
        console.log(username);
        return axios.post(`${urlAuth}/api/`, {
            username,
            password
        })
    }

    getAllUsers() {
        axios.get(urlRegister + "/register");
    }

    getLogin() {
        axios.getLogin(urlRegister + "/login");
    }



    SuccessfulLoginForJwt(username, token) {
        sessionStorage.setItem(USER_NAME_SESSION_ATTRIBUTE_NAME, username)
        this.setupAxiosInterceptors(this.createJWTToken(token))
    }
}
export default new Authentication();


