package com.micro.offer.controller;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.micro.offer.model.Offer;
import com.micro.offer.model.OfferFavorite;
import com.micro.offer.repository.OfferFavoriteRepository;
import com.micro.offer.repository.OfferRepository;
import com.micro.offer.service.OfferFavoriteService;
import com.micro.offer.service.OfferService;
import org.junit.jupiter.api.Test;
import org.hamcrest.Matchers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;


import java.util.*;

import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;



@WebMvcTest(OfferFavoriteController.class)
public class ControllerUnitOfferFavoriteTest {

    @Autowired
    MockMvc mockMvc;


    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    OfferFavoriteService offerFavoriteService;

    @MockBean
    OfferFavoriteRepository offerFavoriteRepository;


    @MockBean
    OfferService offerService;

    @MockBean
    OfferRepository offerRepository;


    @Test
    public void getAllOfferFavoriteTest() throws Exception {
        List<OfferFavorite> offerFavoriteList = new ArrayList<OfferFavorite>();
        Date date = new Date();

        offerFavoriteList.add(new OfferFavorite(1L, "user", date, "23/04/2021", false));
        offerFavoriteList.add(new OfferFavorite(2L, "user", date, "23/04/2021", false));
        given(offerFavoriteService.getOfferFavoriteByAll()).willReturn(offerFavoriteList);

        mockMvc.perform(get("/api/offerFavorites/listOffersFavorites")
                .contentType(MediaType.APPLICATION_JSON)
        ).andDo(print()).andExpect(status().isOk()).andExpect(jsonPath("$", Matchers.hasSize(2)));
    }

    @Test
    public  void getOfferFavoritebyIdTest() throws Exception {
        List<OfferFavorite> offerFavoriteList = new ArrayList<OfferFavorite>();
        Date date = new Date();
        Optional<OfferFavorite> offerFavorite = Optional.of(new OfferFavorite(1L, "user", date,"23/04/2021", false));
        offerFavoriteList.add(new OfferFavorite(1L, "user", date, "23/04/2021",false));
        offerFavoriteList.add(new OfferFavorite(1L, "user", date, "23/04/2021", false));
        given(offerFavoriteService.getOfferFavoriteById(offerFavoriteList.get(1).getId())).willReturn(offerFavorite);

        mockMvc.perform(get("/api/offerFavorites//listOffersFavorites/" + offerFavoriteList.get(1).getId())
                .contentType(MediaType.APPLICATION_JSON)
        ).andDo(print()).andExpect(status().isOk());
    }

    @Test
    public  void getOfferFavoriteUsernameTest() throws Exception {
        List<OfferFavorite> offerFavoriteList = new ArrayList<OfferFavorite>();
        Date date = new Date();
        offerFavoriteList.add(new OfferFavorite(1L, "user", date, "23/04/2021",false));
        offerFavoriteList.add(new OfferFavorite(1L, "user", date, "23/04/2021", false));
        given(offerFavoriteService.getOfferFavoriteByUsername(offerFavoriteList.get(1).getUsername())).willReturn(offerFavoriteList);

        mockMvc.perform(get("/api/offerFavorites/listOffersFavorites/username/" + offerFavoriteList.get(1).getUsername())
                .contentType(MediaType.APPLICATION_JSON)
        ).andDo(print()).andExpect(status().isOk());
    }

    @Test
    public void postOfferFavoriteTest() throws Exception {
        Date date = new Date();
        long numero = 1;

        Offer offer = new Offer(numero, "wesharjob", "premier post", "la ville que tu veux", "wesharejob.co", false, date,"23/04/2021", "user");
        OfferFavorite offerFavorite = new OfferFavorite(1L, "user", date,"23/04/2021", false);
        given(offerService.getOfferById(1L)).willReturn(Optional.of(offer));
        offerFavorite.setOffer(offer);
        given(offerFavoriteService.saveOfferFavorite(offerFavorite)).willReturn(offerFavorite);



        mockMvc.perform(post("/api/offerFavorites/listOffers/" + offerFavorite.getOffer().getId() + "/add-offerFavorite")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(offerFavorite))
        ).andDo(print()).andExpect(status().isOk());
    }



    @Test
    public void deleteOfferFavoriteTest() throws Exception {
        Date date = new Date();
        long numero =1;
        Offer offer = new Offer(numero, "wesharjob", "premier post", "la ville que tu veux", "wesharejob.co", false, date,"23/04/2021", "user");
        OfferFavorite offerFavorite = new OfferFavorite(1L, "user", date,"23/04/2021", false);
        offerFavorite.setOffer(offer);
        given(offerFavoriteService.deleteOfferFavoriteById(offerFavorite.getId())).willReturn(null);
        long id = 1;
        mockMvc.perform(delete("/api/offerFavorites/listOffersFavorites/delete/" + id)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(offer))
        ).andDo(print()).andExpect(status().is2xxSuccessful());

    }
/*
    @Test
    public  void getEmpruntUsernameTest() throws Exception {
        List<Emprunt> empruntList = new ArrayList<Emprunt>();
        Date date = new Date();
        Emprunt emprunt = new Emprunt();
        emprunt.setId(1);
        emprunt.setUsername("pro.brule.thomas@gmail.com");
        emprunt.setEmprunt_retour(false);
        emprunt.setBonus_semaine(false);
        emprunt.setDate_fin_prevu(date);
        emprunt.setDate_emprunt(date);
        emprunt.setDate_semaine_bonus(date);
        Emprunt empruntM = new Emprunt();
        empruntM.setId(1);
        empruntM.setUsername("pro.brule.thomas@gmail.com");
        empruntM.setEmprunt_retour(false);
        empruntM.setBonus_semaine(false);
        empruntM.setDate_fin_prevu(date);
        empruntM.setDate_emprunt(date);
        empruntM.setDate_semaine_bonus(date);
        empruntList.add(empruntM);
        empruntList.add(emprunt);
        given(empruntService.getEmpruntByUsername(empruntList.get(1).getUsername())).willReturn(empruntList);

        mockMvc.perform(get("/api/listeEmpruntsEncours/" + empruntList.get(1).getUsername())
                .contentType(MediaType.APPLICATION_JSON)
        ).andDo(print()).andExpect(status().isOk());
    }




    @Test
    public  void getEmpruntbyIdTest() throws Exception {
        List<Emprunt> empruntList = new ArrayList<Emprunt>();
        Date date = new Date();
        Emprunt emprunt = new Emprunt();
        emprunt.setId(1);
        emprunt.setUsername("pro.brule.thomas@gmail.com");
        emprunt.setEmprunt_retour(false);
        emprunt.setBonus_semaine(false);
        emprunt.setDate_fin_prevu(date);
        emprunt.setDate_emprunt(date);
        emprunt.setDate_semaine_bonus(date);
        Optional<Emprunt> empruntId = Optional.of(emprunt);
        Emprunt emprunt1 = new Emprunt();
        emprunt1.setId(1);
        emprunt1.setUsername("pro.brule.thomas@gmail.com");
        emprunt1.setEmprunt_retour(false);
        emprunt1.setBonus_semaine(false);
        emprunt1.setDate_fin_prevu(date);
        emprunt1.setDate_emprunt(date);
        emprunt1.setDate_semaine_bonus(date);
        Emprunt emprunt2 = new Emprunt();
        emprunt2.setId(1);
        emprunt2.setUsername("pro.brule.thomas@gmail.com");
        emprunt2.setEmprunt_retour(false);
        emprunt2.setBonus_semaine(false);
        emprunt2.setDate_fin_prevu(date);
        emprunt2.setDate_emprunt(date);
        emprunt2.setDate_semaine_bonus(date);
        empruntList.add(emprunt1);
        empruntList.add(emprunt2);
        given(empruntService.getEmpruntById((int) empruntList.get(1).getId())).willReturn(empruntId);

        mockMvc.perform(get("/api/listeEmpruntsEncours/id/" + empruntList.get(1).getId())
                .contentType(MediaType.APPLICATION_JSON)
        ).andDo(print()).andExpect(status().isOk());
    }


    @Test
    public  void nouvelleEmpruntTest() throws Exception {
        Date date = new Date();
        int numero = 1;
        Livre livre = new Livre();
        livre.setId(1);
        livre.setUsername("adminbiblio");
        livre.setAuteur("j.k rowling");
        livre.setTitre("harry potter a l ecole de sorcier");
        livre.setNombre_disponible(10);
        livre.setQuantite_reservation(10);
        livre.setQuantite_livre(10);
        livre.setDate_emprunt(date);
        livre.setDate_emprunt_show("2021-06-08");


        Emprunt emprunt = new Emprunt();
        emprunt.setId(1);
        emprunt.setUsername("pro.brule.thomas@gmail.com");
        emprunt.setEmprunt_retour(false);
        emprunt.setBonus_semaine(false);
        emprunt.setDate_fin_prevu(date);
        emprunt.setDate_emprunt(date);
        emprunt.setDate_semaine_bonus(date);

        //   given(livreRepository.save(livre)).willReturn(livre);
        given(livreService.getLivreById(1)).willReturn(Optional.of(livre));
        //    offerFavorite.setOffer(offer);
        emprunt.setLivre(livre);
        given(empruntService.saveEmprunt(emprunt)).willReturn(emprunt);


        mockMvc.perform(post("/api/"+livre.getId()+"/nouvelleEmprunt")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(livre))
        ).andDo(print()).andExpect(status().is2xxSuccessful());
    }


    @Test
    public  void nouvelleEmpruntTestFailedLivreDisponible0() throws Exception {
        Date date = new Date();
        int numero = 1;

        Livre livre = new Livre();
        livre.setId(1);
        livre.setUsername("adminbiblio");
        livre.setAuteur("j.k rowling");
        livre.setTitre("harry potter a l ecole de sorcier");
        livre.setNombre_disponible(10);
        livre.setQuantite_reservation(10);
        livre.setQuantite_livre(10);
        livre.setDate_emprunt(date);
        livre.setDate_emprunt_show("2021-06-08");



        Emprunt emprunt = new Emprunt();
        emprunt.setId(1);
        emprunt.setUsername("pro.brule.thomas@gmail.com");
        emprunt.setEmprunt_retour(false);
        emprunt.setBonus_semaine(false);
        emprunt.setDate_fin_prevu(date);
        emprunt.setDate_emprunt(date);
        emprunt.setDate_semaine_bonus(date);
        //   given(livreRepository.save(livre)).willReturn(livre);
        given(livreService.getLivreById(1)).willReturn(Optional.of(livre));
        //    offerFavorite.setOffer(offer);
        emprunt.setLivre(livre);
        livre.setNombre_disponible(0);
        if(livre.getNombre_disponible() == 0) {
            mockMvc.perform(post("/api/" + livre.getId() + "/nouvelleEmprunt")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(objectMapper.writeValueAsString(livre))
            ).andDo(print()).andExpect(status().is2xxSuccessful());
            System.out.println("retour emprunt sans save");
        }else {
            given(empruntService.saveEmprunt(emprunt)).willReturn(emprunt);
            mockMvc.perform(post("/api/"+livre.getId()+"/nouvelleEmprunt")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(objectMapper.writeValueAsString(livre))
            ).andDo(print()).andExpect(status().is4xxClientError());
            System.out.println("save livre");
        }
    }




    @Test
    public  void  updateProlongerEmpruntTest() throws Exception {
        int id = 1;
        Date date = new Date();
        //   Livre livre = new Livre(1l,"admin1biblio", "j.k rowling","harry potter a l ecole de sorcier",10, 0,10,false,date,"2021-06-08");


        Livre livre = new Livre();
        livre.setId(1);
        livre.setUsername("adminbiblio");
        livre.setAuteur("j.k rowling");
        livre.setTitre("harry potter a l ecole de sorcier");
        livre.setNombre_disponible(10);
        livre.setQuantite_reservation(10);
        livre.setQuantite_livre(10);
        livre.setDate_emprunt(date);
        livre.setDate_emprunt_show("2021-06-08");

        Emprunt emprunt = new Emprunt();
        emprunt.setId(1);
        emprunt.setUsername("pro.brule.thomas@gmail.com");
        emprunt.setEmprunt_retour(false);
        emprunt.setBonus_semaine(false);
        emprunt.setDate_fin_prevu(date);
        emprunt.setDate_emprunt(date);
        emprunt.setDate_semaine_bonus(date);
        given(empruntService.getEmpruntById(1)).willReturn(Optional.of(emprunt));
        //      emprunt.setLivre(livre);
        emprunt.setEmprunt_retour(false);
        given(empruntService.updateEmprunt(emprunt)).willReturn(emprunt);
        mockMvc.perform(put("/api/listeEmpruntsEncours/prolongerEmprunt/" + id)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(emprunt)))
                .andDo(print())
                .andExpect(status().is2xxSuccessful());
    }


    @Test
    public  void updateEmpruntTestFailedDate() throws Exception {
        int id = 1;
        Date date = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.WEEK_OF_MONTH, 2);
        Date date2 = calendar.getTime();

        Date date4week = new Date();
        Calendar c = Calendar.getInstance();
        c.setTime(date4week);
        c.add(Calendar.WEEK_OF_MONTH, 4);
        Date date4 = c.getTime();


        Emprunt emprunt = new Emprunt();
        emprunt.setId(1);
        emprunt.setUsername("pro.brule.thomas@gmail.com");
        emprunt.setEmprunt_retour(false);
        emprunt.setBonus_semaine(false);
        emprunt.setDate_fin_prevu(date);
        emprunt.setDate_emprunt(date);
        emprunt.setDate_semaine_bonus(date);

        given(empruntService.getEmpruntById(1)).willReturn(Optional.of(emprunt));
        //      emprunt.setLivre(livre);
        emprunt.setEmprunt_retour(false);
        emprunt.setDate_emprunt(date2);
        if(date4.compareTo(date2) > 0) {
            mockMvc.perform(put("/api/listeEmpruntsEncours/prolongerEmprunt/" + id)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(objectMapper.writeValueAsString(emprunt)))
                    .andDo(print())
                    .andExpect(status().is2xxSuccessful());
            System.out.println("il est pas possible de prolonger l'emprunt");
        } else {
            given(empruntService.updateEmprunt(emprunt)).willReturn(emprunt);
            mockMvc.perform(put("/api/listeEmpruntsEncours/prolongerEmprunt/" + id)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(objectMapper.writeValueAsString(emprunt)))
                    .andDo(print())
                    .andExpect(status().is2xxSuccessful());
            System.out.println("l'emprunt est prolonger");
        }
    }


    @Test
    public  void deleteEmpruntTest() throws Exception {
        Date date = new Date();
        int numero = 1;



        Livre livre = new Livre();
        livre.setId(1);
        livre.setUsername("adminbiblio");
        livre.setAuteur("j.k rowling");
        livre.setTitre("harry potter a l ecole de sorcier");
        livre.setNombre_disponible(10);
        livre.setQuantite_reservation(10);
        livre.setQuantite_livre(10);
        livre.setDate_emprunt(date);
        livre.setDate_emprunt_show("2021-06-08");

        Emprunt emprunt = new Emprunt();
        emprunt.setId(1);
        emprunt.setUsername("pro.brule.thomas@gmail.com");
        emprunt.setEmprunt_retour(false);
        emprunt.setBonus_semaine(false);
        emprunt.setDate_fin_prevu(date);
        emprunt.setDate_emprunt(date);
        emprunt.setDate_semaine_bonus(date);

        emprunt.setLivre(livre);
        given(empruntService.getEmpruntById(1)).willReturn(Optional.of(emprunt));
        int id = 1;
        mockMvc.perform(delete("/api/deleteEmprunt/" + id)
                .contentType(MediaType.APPLICATION_JSON)
        ).andDo(print()).andExpect(status().is2xxSuccessful());
    }*/



}
