package com.micro.offer.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.micro.offer.model.Offer;
import com.micro.offer.repository.OfferRepository;
import com.micro.offer.service.OfferService;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;




@WebMvcTest(OfferController.class)
public class ControllerUnitOfferTest {

    @Autowired
    MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    OfferService offerService;


    @MockBean
    OfferRepository offerRepository;


    @Test
    public  void getAllOfferTest() throws Exception {
        List<Offer> offerList = new ArrayList<Offer>();
        Date date = new Date();
        long numero = 1;
        long numero2 = 2;
        offerList.add(new Offer(numero, "wesharjob", "premier post", "la ville que tu veux", "wesharejob.co", false, date,"23/04/2021", "user"));
        offerList.add(new Offer(numero2, "wesharjob", "premier post", "la ville que tu veux", "wesharejob.co", false, date,"23/04/2021", "user"));
        given(offerService.getAllOffer()).willReturn(offerList);

        mockMvc.perform(get("/api/offers/listOffers")
                .contentType(MediaType.APPLICATION_JSON)
        ).andDo(print()).andExpect(status().isOk()).andExpect(jsonPath("$", Matchers.hasSize(2)));
    }


    @Test
    public  void getOfferUsernameTest() throws Exception {
        List<Offer> offerList = new ArrayList<Offer>();
        Date date = new Date();
        long numero = 1;
        long numero2 = 2;
        offerList.add(new Offer(numero, "wesharjob", "premier post", "la ville que tu veux", "wesharejob.co", false, date,"23/04/2021", "user"));
        offerList.add(new Offer(numero2, "wesharjob", "premier post", "la ville que tu veux", "wesharejob.co", false, date,"23/04/2021", "user"));
        given(offerService.getOfferByUsername(offerList.get(1).getUsername())).willReturn(offerList);

        mockMvc.perform(get("/api/offers/listOffers/username/" + offerList.get(1).getUsername())
                .contentType(MediaType.APPLICATION_JSON)
        ).andDo(print()).andExpect(status().isOk());
    }


    @Test
    public  void getOfferbyIdTest() throws Exception {
        List<Offer> offerList = new ArrayList<Offer>();
        Date date = new Date();
        long numero = 1;
        long numero2 = 2;
        Optional<Offer> offerId = Optional.of(new Offer(numero, "wesharjob", "premier post", "la ville que tu veux", "wesharejob.co", false, date,"23/04/2021", "user"));
        offerList.add(new Offer(numero, "wesharjob", "premier post", "la ville que tu veux", "wesharejob.co", false, date,"23/04/2021", "user"));
        offerList.add(new Offer(numero2, "wesharjob", "premier post", "la ville que tu veux", "wesharejob.co", false, date,"23/04/2021", "user"));
        given(offerService.getOfferById(offerList.get(1).getId())).willReturn(offerId);

        mockMvc.perform(get("/api/offers/listOffers/id/" + offerList.get(1).getId())
                .contentType(MediaType.APPLICATION_JSON)
        ).andDo(print()).andExpect(status().isOk());
    }



    @Test
    public  void postOfferTest() throws Exception {
        Date date = new Date();
        long numero = 1;
        Offer offer = new Offer(numero, "wesharjob", "premier post", "la ville que tu veux", "wesharejob.co", false, date,"23/04/2021", "user");

        given(offerService.saveOffer(offer)).willReturn(offer);

        mockMvc.perform(post("/api/offers/listOffers/post")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(offer))
        ).andDo(print()).andExpect(status().isCreated());
    }



    @Test
    public  void rechercheOfferTest() throws Exception {

        List<Offer> offerList = new ArrayList<Offer>();
        Date date = new Date();
        long numero = 1;
        long numero2 = 2;
        offerList.add(new Offer(numero, "wesharjob", "premier post", "la ville que tu veux", "wesharejob.co", false, date,"23/04/2021", "user"));
        offerList.add(new Offer(numero2, "wesharjob", "premier post", "la ville que tu veux", "wesharejob.co", false, date,"23/04/2021", "user"));


        given(offerService.getOfferByCompanyOrPostOrCity(offerList.get(1).getCompany(),offerList.get(1).getPost(), offerList.get(1).getCity())).willReturn(offerList);

        mockMvc.perform(get("/api/offers/listOffers/search")
                .contentType(MediaType.APPLICATION_JSON)
        ).andDo(print()).andExpect(status().isOk());
    }


    @Test
    public  void updateOfferTest() throws Exception {
        long id = 1;
        Date date = new Date();
        Offer offer = new Offer(id, "wesharjob", "premier post", "la ville que tu veux", "wesharejob.co", false, date,"23/04/2021", "user");
        given(offerRepository.findById(offer.getId())).willReturn(Optional.of(offer));
        offer.setPost("deuxieme post");
        given(offerService.updateOffer(offer)).willReturn(offer);
        mockMvc.perform(put("/api/offers/listOffers/update/" + id)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(offer)))
                .andDo(print())
                .andExpect(status().is2xxSuccessful());
    }


    @Test
    public  void deleteOfferTest() throws Exception {
        Date date = new Date();
        long numero = 1;
        Offer offer = new Offer(numero, "wesharjob", "premier post", "la ville que tu veux", "wesharejob.co", false, date,"23/04/2021", "user");
        given(offerService.deleteOfferById(offer.getId())).willReturn(null);
        long id = 1;
        mockMvc.perform(delete("/api/offers/listOffers/delete/" + id)
                .contentType(MediaType.APPLICATION_JSON)
        ).andDo(print()).andExpect(status().is2xxSuccessful());
    }

/*
    public  void rechercheLivreTest() throws Exception {

        List<Livre> livreList = new ArrayList<Livre>();
        Date date = new Date();
        int numero = 1;
        int numero2 = 2;
        Livre livre = new Livre();
        livre.setId(1);
        livre.setUsername("adminbiblio");
        livre.setAuteur("j.k rowling");
        livre.setTitre("harry potter a l ecole de sorcier");
        livre.setNombre_disponible(10);
        livre.setQuantite_reservation(10);
        livre.setDate_emprunt(date);
        livre.setDate_emprunt_show("2021-06-08");
        Livre livre1 = new Livre();
        livre1.setId(2);
        livre1.setUsername("adminbiblio");
        livre1.setAuteur("j.k rowling");
        livre1.setTitre("harry potter a l ecole de sorcier");
        livre1.setNombre_disponible(10);
        livre1.setQuantite_reservation(10);
        livre1.setDate_emprunt(date);
        livre1.setDate_emprunt_show("2021-06-08");
        livreList.add(livre);
        livreList.add(livre1);

        given(livreRepository.findByAuteurOrTitre(livreList.get(1).getTitre(),livreList.get(1).getAuteur())).willReturn(livreList);

        mockMvc.perform(post("/api/Livre/listeLivres/recherche")
                .contentType(MediaType.APPLICATION_JSON)
        ).andDo(print()).andExpect(status().isOk());
    }

    @Test
    public  void getAllLivreTest() throws Exception {
        List<Livre> livreList = new ArrayList<Livre>();
        Date date = new Date();
        int numero = 1;
        int numero2 = 2;
        Livre livre = new Livre();
        livre.setId(1);
        livre.setUsername("adminbiblio");
        livre.setAuteur("j.k rowling");
        livre.setTitre("harry potter a l ecole de sorcier");
        livre.setNombre_disponible(10);
        livre.setQuantite_reservation(10);
        livre.setQuantite_livre(10);
        livre.setDate_emprunt(date);
        livre.setDate_emprunt_show("2021-06-08");
        Livre livre1 = new Livre();
        livre1.setId(2);
        livre1.setUsername("adminbiblio");
        livre1.setAuteur("j.k rowling");
        livre1.setTitre("harry potter a l ecole de sorcier");
        livre1.setNombre_disponible(10);
        livre1.setQuantite_reservation(10);
        livre1.setDate_emprunt(date);
        livre1.setDate_emprunt_show("2021-06-08");
        livreList.add(livre);
        livreList.add(livre1);
        given(livreRepository.findAll()).willReturn(livreList);

        mockMvc.perform(get("/api/Livre/listeLivres")
                .contentType(MediaType.APPLICATION_JSON)
        ).andDo(print()).andExpect(status().isOk()).andExpect(jsonPath("$", Matchers.hasSize(2)));
    }*/




}

