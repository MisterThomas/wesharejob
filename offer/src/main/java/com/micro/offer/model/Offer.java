package com.micro.offer.model;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "offer")
@Data
public class Offer {

    // voir si on travaille avec javax.presitance ou hibernate pourherite de plusieurs classes cela peut poser le probleme

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "username")
    private String username;

    @Column(name = "company")
    private String company;

    @Column(name = "post")
    private String post;

    @Column(name = "city")
    private String city;

    @Column(name = "url")
    private String url;

    @Column(nullable = false, name = "is_active")
    private Boolean is_active;



    @Column(name = "date_batch")
    private Date dateBatch = new Date();

    @Column(name = "date_scrap_offer")
    private String date_scrap_offer;

    @JsonCreator
    public Offer(@JsonProperty("company")String company, @JsonProperty("post")String post, @JsonProperty("city")String city, @JsonProperty("url")String url, @JsonProperty("is_active")Boolean is_active, @JsonProperty("dateScrapOffer")String date_scrap_offer, @JsonProperty("dateBatch") Date dateBatch, @JsonProperty("username") String username) {
        this.company = company;
        this.post = post;
        this.city = city;
        this.url = url;
        this.is_active = is_active;
        this.dateBatch = dateBatch;
        this.date_scrap_offer = date_scrap_offer;
        this.username = username;
    }

    public Offer(Long id,String company, String post, String city, String url, Boolean is_active, Date dateBatch, String date_scrap_offer, String username) {
        this.id = id;
        this.company = company;
        this.post = post;
        this.city = city;
        this.url = url;
        this.is_active = is_active;
        this.dateBatch = dateBatch;
        this.date_scrap_offer = date_scrap_offer;
        this.username = username;
    }

    public Offer() {

    }

    @JsonIgnore
    @OneToMany(mappedBy = "offer", cascade = CascadeType.ALL)
    private Set<OfferFavorite> offerFavorite = new HashSet<>();

    public Set<OfferFavorite> getOfferFavorite() {
        return offerFavorite;
    }

    public void setOfferFavorite(Set<OfferFavorite> offerFavorite) {
        this.offerFavorite = offerFavorite;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }


    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getPost() {
        return post;
    }

    public void setPost(String post) {
        this.post = post;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Boolean getIs_active() {
        return is_active;
    }

    public void setIs_active(Boolean is_active) {
        this.is_active = is_active;
    }

    public Date getDateBatch() {
        return dateBatch;
    }

    public void setDateBatch(Date dateBatch) {
        this.dateBatch = dateBatch;
    }

    public String getDate_scrap_offer() {
        return date_scrap_offer;
    }

    public void setDate_scrap_offer(String date_scrap_offer) {
        this.date_scrap_offer = date_scrap_offer;
    }
}
