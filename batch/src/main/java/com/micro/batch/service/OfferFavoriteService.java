package com.micro.batch.service;

import com.micro.batch.repository.OfferFavoriteRepository;
import com.micro.batch.model.OfferFavorite;
import com.micro.batch.model.Offer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Service
public class OfferFavoriteService implements IOfferFavoriteService {

    @Autowired
    OfferFavoriteRepository offerFavoriteRepository;

    @Override
    public List<OfferFavorite> getOfferFavoriteByAll() {
        return offerFavoriteRepository.findAll();
    }

    @Override
    public List<OfferFavorite> getOfferFavoriteByUsername(String offreFavorite) {
        return  offerFavoriteRepository.findByUsername(offreFavorite);
    }

    @Override
    public List<OfferFavorite> OfferFavoritebyDate(String date) {
        return offerFavoriteRepository.findALLByAndDateScrapOffer(date);
    }

    @Override
    public Optional<OfferFavorite> getOfferFavoriteById(long id) {
        return offerFavoriteRepository.findById(id);
    }


    @Override
    public OfferFavorite saveOfferFavorite(OfferFavorite offerFavorite) {
        return offerFavoriteRepository.save(offerFavorite);
    }

    @Override
    public OfferFavorite updateOfferFavorite(OfferFavorite offerFavorite) {
        offerFavorite = offerFavoriteRepository.save(offerFavorite);
        return offerFavorite;
    }

    @Override
    public void deleteOfferFavorite(long id) {
        Optional<OfferFavorite> offerFavorite = offerFavoriteRepository.findById(id);
        if (offerFavorite.isPresent()) {
            offerFavoriteRepository.delete(offerFavorite.get());
        }
    }

    @Override
    public Optional<OfferFavorite> deleteOfferFavoriteById(long id) {
        Optional<OfferFavorite> offerFavorite = offerFavoriteRepository.findById(id);
        if (offerFavorite.isPresent()) {
            offerFavoriteRepository.delete(offerFavorite.get());
            return offerFavorite;

        }
        return Optional.empty();
    }
}
