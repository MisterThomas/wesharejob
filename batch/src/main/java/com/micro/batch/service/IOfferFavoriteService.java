package com.micro.batch.service;

import com.micro.batch.model.OfferFavorite;
import com.micro.batch.model.Offer;
import org.springframework.stereotype.Service;

import java.text.DateFormat;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Optional;
@Service
public interface IOfferFavoriteService {

    public List<OfferFavorite>  getOfferFavoriteByAll();

    public List<OfferFavorite> getOfferFavoriteByUsername(String offre);


    List<OfferFavorite> OfferFavoritebyDate(String date);


    Optional<OfferFavorite> getOfferFavoriteById(long id);

    Optional<OfferFavorite> deleteOfferFavoriteById(long id);


    OfferFavorite saveOfferFavorite(OfferFavorite offerFavorite);


    OfferFavorite updateOfferFavorite(OfferFavorite offerFavorite);

    void deleteOfferFavorite(long id);
}
