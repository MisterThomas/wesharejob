package com.micro.batch.service;

import com.micro.batch.model.Offer;

import java.util.List;
import java.util.Optional;

public interface IOfferService {
    public List<Offer> getAllOffer();

    public List<Offer> getOfferByUsername(String offer);


    List<Offer> getOfferByCompanyOrPostOrCity(String company,String post,String city);

    List<Offer> OfferbyDate(String date);

    Optional<Offer> getOfferById(long id);


    //   Optional<Offer> getOfferById(long id);


    Optional<Offer> deleteOfferById(long id);


    Offer saveOffer(Offer offer);


    Offer updateOffer(Offer offer);


    void deleteOffer(long id);

}
