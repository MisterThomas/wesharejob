package com.micro.batch.repository;



import com.micro.batch.model.OfferFavorite;
import com.micro.batch.model.Offer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
//import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface OfferFavoriteRepository extends JpaRepository<OfferFavorite, Long> {


    List<OfferFavorite> findByUsername(String offer);


    @Query("SELECT o FROM OfferFavorite  o WHERE o.date_scrap_offer = o.date_scrap_offer")
    List<OfferFavorite> findALLByAndDateScrapOffer(String date);



}
