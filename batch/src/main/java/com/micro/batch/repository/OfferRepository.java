package com.micro.batch.repository;

import com.micro.batch.model.Offer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
//import org.springframework.stereotype.Repository;

import java.util.List;

//@Repository
public interface OfferRepository extends JpaRepository<Offer, Long> {

    List<Offer> findByUsername(String offer);

    List<Offer> findByCity (String city);


    List<Offer> findByCompanyOrCityOrPost(String company,String city,String post);

    @Query("SELECT o FROM Offer  o WHERE o.date_scrap_offer = o.date_scrap_offer")
    List<Offer> findALLByAndDateScrapOffer(String date);


}
