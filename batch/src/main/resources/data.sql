
livre_id

INSERT INTO livre (id, auteur,date_emprunt,date_emprunt_show,quantite_livre,quantite_reservation, nombre_disponible, titre, username)
VALUES (1, 'j.k rowling','Sun July 11 20:40:12 CEST 2021','11-07-2021',  10,0,2, 'harry potter a l ecole de sorcier', 'admin1biblio');

INSERT INTO livre (id, auteur,date_emprunt,date_emprunt_show,quantite_livre,quantite_reservation, nombre_disponible, titre, username)
VALUES (2, 'anthony horowtiz','Sun July 11 20:40:12 CEST 2021','11-07-2021',  10,0,0, 'l ile du crane', 'admin1biblio');


INSERT INTO livre (id, auteur,date_emprunt,date_emprunt_show,quantite_livre,quantite_reservation, nombre_disponible, titre, username)
VALUES (3, 'shakespeare', 'Sun July 11 20:40:12 CEST 2021','11-07-2021', 10,3,0, 'hamlet', 'admin1biblio');


INSERT INTO livre (id, auteur,date_emprunt,date_emprunt_show,quantite_livre,quantite_reservation, nombre_disponible, titre, username)
VALUES (4, 'François Rabelais','Sun July 11 20:40:12 CEST 2021','11-07-2021', 10,1,0, 'gargantua', 'admin1biblio');

INSERT INTO emprunt (id,livre_id, emprunt_retour,bonus_semaine, date_emprunt, date_fin_prevu,date_fin_prevu_show, date_semaine_bonus, username)
VALUES (1, 1, false,false, 'Sun July 11 20:40:12 CEST 2021', 'Sun August 1 20:40:12 CEST 2021','11-07-2021', 'Sun August 8 20:40:12 CEST 2021', 'thomas');


INSERT INTO emprunt (id,livre_id, emprunt_retour,bonus_semaine, date_emprunt, date_fin_prevu,date_fin_prevu_show, date_semaine_bonus, username)
VALUES (2,2, false,false, 'Sun July 11 20:40:12 CEST 2021', 'Sun August 1 20:40:12 CEST 2021','11-07-2021', 'Sun August 8 20:40:12 CEST 2021', 'thomas');



INSERT INTO emprunt (id,livre_id, emprunt_retour,bonus_semaine, date_emprunt, date_fin_prevu, date_semaine_bonus, username)
VALUES (3, 3, false,false, 'Sun July 11 20:40:12 CEST 2021', 'Sun August 1 20:40:12 CEST 2021', 'Sun August 8 20:40:12 CEST 2021', 'pro.brule.thomas@gmail.com');


INSERT INTO emprunt (id,livre_id, emprunt_retour,bonus_semaine, date_emprunt, date_fin_prevu, date_semaine_bonus, username)
VALUES (4,4, false,false, 'Sun July 11 20:40:12 CEST 2021', 'Sun August 1 20:40:12 CEST 2021', 'Sun August 8 20:40:12 CEST 2021', 'pro.brule.thomas@gmail.com');


INSERT INTO emprunt (id,livre_id, emprunt_retour,bonus_semaine, date_emprunt, date_fin_prevu, date_semaine_bonus, username)
VALUES (5, 3, false,false, 'Sun July 11 20:40:12 CEST 2021', 'Sun August 1 20:40:12 CEST 2021', 'Sun August 8 20:40:12 CEST 2021', 'pro.brule.thomas@gmail.com');


INSERT INTO emprunt (id,livre_id, emprunt_retour,bonus_semaine, date_emprunt, date_fin_prevu, date_semaine_bonus, username)
VALUES (6,4, false,false, 'Sun July 11 20:40:12 CEST 2021', 'Sun August 1 20:40:12 CEST 2021', 'Sun August 8 20:40:12 CEST 2021', 'pro.brule.thomas@gmail.com');


INSERT INTO emprunt (id,livre_id, emprunt_retour,bonus_semaine, date_emprunt, date_fin_prevu, date_semaine_bonus, username)
VALUES (7, 3, false,false, 'Sun July 11 20:40:12 CEST 2021', 'Sun August 1 20:40:12 CEST 2021', 'Sun August 8 20:40:12 CEST 2021', 'pro.brule.thomas@gmail.com');


INSERT INTO emprunt (id,livre_id, emprunt_retour,bonus_semaine, date_emprunt, date_fin_prevu, date_semaine_bonus, username)
VALUES (8,4, false,false, 'Sun July 11 20:40:12 CEST 2021', 'Sun August 1 20:40:12 CEST 2021', 'Sun August 8 20:40:12 CEST 2021', 'pro.brule.thomas@gmail.com');




INSERT INTO reservation (id,livre_id, reservation_choix,numeroreservation,date_reservation, date_fin_livre_reservation,date_resa, username)
VALUES (1,3, false, 1,'Sun July 11 20:40:12 CEST 2021', 'Sun August 1 20:40:12 CEST 2021', '11-07-2021', 'pro.brule.thomas@gmail.com');

INSERT INTO reservation (id,livre_id, reservation_choix,numeroreservation,date_reservation, date_fin_livre_reservation,date_resa,username)
VALUES (4,3, false, 1,'Sun July 11 20:40:12 CEST 2021', 'Sun August 1 20:40:12 CEST 2021','11-07-2021', 'pro.brule.thomas@gmail.com');

INSERT INTO reservation (id,livre_id, reservation_choix,numeroreservation,date_reservation, date_fin_livre_reservation,date_resa, username)
VALUES (3,3, false, 2,'Sun July 11 20:40:12 CEST 2021', 'Sun August 1 20:40:12 CEST 2021','11-07-2021', 'test2@email.com');

INSERT INTO  reservation (id,livre_id, reservation_choix,numeroreservation,date_reservation,date_fin_livre_reservation,date_resa,username)
VALUES (2,4, false, 3,'Sun July 11 20:40:12 CEST 2021', 'Sun August 1 20:40:12 CEST 2021','11-07-2021',  'test2@email.com');