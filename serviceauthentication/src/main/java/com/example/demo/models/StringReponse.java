package com.example.demo.models;

public class StringReponse {

    private String response;

    public StringReponse(String response) {
        this.response = response;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }
}
