package com.example.demo.services;

import com.example.demo.models.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.models.UserEntity;
import com.example.demo.repositories.UserRepository;

import java.util.Collection;
import java.util.HashSet;

@Service 
@Transactional
public class AccountService implements IAccountService {
	
	@Autowired
	private PasswordEncoder passwordEncoder;
	
	@Autowired 
	private UserRepository userRepository;



	@Override
	public UserEntity saveUserEntity(UserEntity user) {
		String hashPassword = passwordEncoder.encode(user.getPassword());
		user.setRole(Role.USER);
		userRepository.save(user);

		user.setPassword(hashPassword);
		return userRepository.save(user);	
	}



	@Override
	public UserEntity findUserByUsername(String username) {
 		return userRepository.findByUsername(username);
	}

}
