package com.example.demo.services;

import com.example.demo.models.UserEntity;

public interface IAccountService {
	
	UserEntity saveUserEntity(UserEntity user);
	
	public UserEntity findUserByUsername(String username);
}
