package com.example.demo.models;


import org.apache.logging.log4j.core.Logger;
import org.junit.Assert;
import org.junit.jupiter.api.*;

import java.text.MessageFormat;
import java.time.Duration;
import java.time.Instant;
import java.util.Collection;
import java.util.Collections;
import java.util.List;


@Tag("UserTest")
@DisplayName("Check User entity")
public class UserEntityTest {


    private UserEntity userEntity;
    private static Instant startedAt;

    //utiliser la classe LoggingExtension au lieu de System.out.println
    private Logger logger;

    public void setLogger(Logger logger) {
        this.logger = logger;
    }

    //appel avant tout les tests pour demarrer le chrono
    @BeforeAll
    public static void initStartingTime() {
        System.out.println("Appel avant tout les tests");
        startedAt = Instant.now();
    }

    //Calcul de delai antre début et fin des tests
    @AfterAll
    public static void testDuration() {
        System.out.println("Appel après tout les tests");
        Instant endedAt = Instant.now();
        long duration = Duration.between(startedAt, endedAt).toMillis();
        System.out.println(MessageFormat.format("Durée des test: {0} ms", duration));

    }

    //appel avant chaque test
    @BeforeEach
    public void initUser() {
        userEntity = new UserEntity();
        System.out.println("Appel avant chaque test");
    }

    //appel après chaque test
    @AfterEach
    public void undefUser() {
        System.out.println("Appel après chaque test");
        userEntity = null;
    }

    @Test
    public void testUser() {
        //GIVEN
        final long id = 2;
        final String email = "test.com";
        final String username = "test1";
        final String password = "test";
        final Boolean active = false;
        final  Role role;


        // WHEN
        UserEntity userTest = new UserEntity();
        userTest.setId(id);
        userTest.setEmail(email);
        userTest.setUsername(username);
        userTest.setPassword(password);
        userTest.setActive(active);
        userTest.setRole(Role.USER);

        // THEN
        Assert.assertEquals(userTest.toString(),
                "UserEntity "+
                        "[id=2, email=test.com, username=test1, password=test, active=false, roles=USER]");
        System.out.println(userTest.getRole());
    }
}
