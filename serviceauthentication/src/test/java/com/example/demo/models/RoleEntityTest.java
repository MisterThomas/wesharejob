package com.example.demo.models;

import org.apache.logging.log4j.core.Logger;
import org.junit.jupiter.api.*;

import java.text.MessageFormat;
import java.time.Duration;
import java.time.Instant;

@Tag("RoleTest")
@DisplayName("Check Role entity")
public class RoleEntityTest {

/*
	private RoleEntity roleEntity;

	private static Instant startedAt;

	//utiliser la classe LoggingExtension au lieu de System.out.println
	private Logger logger;

	public void setLogger(Logger logger) {
		this.logger = logger;
	}

	//appel avant tout les tests pour demarrer le chrono
	@BeforeAll
	public static void initStartingTime() {
		System.out.println("Appel avant tout les tests");
		startedAt = Instant.now();
	}

	//Calcul de delai antre début et fin des tests
	@AfterAll
	public static void testDuration() {
		System.out.println("Appel après tout les tests");
		Instant endedAt = Instant.now();
		long duration = Duration.between(startedAt, endedAt).toMillis();
		System.out.println(MessageFormat.format("Durée des test: {0} ms", duration));

	}

	//appel avant chaque test
	@BeforeEach
	public void initOffer() {
		roleEntity = new RoleEntity();
		System.out.println("Appel avant chaque test");
	}

	//appel après chaque test
	@AfterEach
	public void undefOffer() {
		System.out.println("Appel après chaque test");
		roleEntity = null;
	}


	@Test
	public void testRole() {
		final long id = 2;
		final String roleName = "admin";


		// WHEN
		RoleEntity roleTest = new RoleEntity();
		roleTest.setId(id);
		roleTest.setRoleName(roleName);

		// THEN
		Assertions.assertEquals(roleTest.toString(),
				"RoleEntity "  +
						"[id=2, roleName=admin]");
	}
*/

}
